# Control System Theory - Educational Platform Frontend
Frontend part of a C.S.T.E.P. project realized for the Mechatronic Design course at the AGH University.
Build with TypeScript and Angular.

## Pre requirements
 - node.js
 - yarn
 - Angular CLI

## Installing requirements
```
cd /project_dir
yarn
```

## Starting app - development
```
cd /project_dir
ng serve
```
App will be hosted on http://localhost:4200/ unless specified otherwise.

## Running tests
```
cd /project_dir
ng test
ng e2e
```
