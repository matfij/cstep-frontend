fs = require('fs')

const index_path = 'dist/index.html';
let correctData = '';

fs.readFile(index_path, 'utf-8', function (err, incorrectData) {
  const incorrectScriptType = 'type="module"';
  const correctScriptType = 'type="application/javascript"';
  correctData = incorrectData.split(incorrectScriptType).join(correctScriptType);

  fs.writeFileSync(index_path, correctData, 'utf-8', (err) => {
    if (err) throw err;
    console.log('The file has been saved!');
  });
});
