import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClient, HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { TranslateModule, TranslateLoader, TranslateService } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ThemeAppModule } from './@theme-app/theme-app.module';
import { JwtInterceptor } from './api/jwt.interceptor';
import { RegisterComponent } from './pages/common/register/register.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ChartsModule } from 'ng2-charts';
import { HomeComponent as StudentHomeComponent } from './pages/student/home/home.component';
import { TasksComponent as StudentTasksComponent } from './pages/student/tasks/tasks.component';
import { HomeComponent as TeacherHomeComponent } from './pages/teacher/home/home.component';
import { TasksComponent as TeacherTasksComponent } from './pages/teacher/tasks/tasks.component';
import { LoginComponent } from './pages/common/login/login.component';
import { SettingsComponent } from './pages/common/settings/settings.component';
import { ControlPanelComponent } from './pages/common/control-panel/control-panel.component';
import { InputTextareaComponent } from './components/formly/input-textarea/input-textarea.component';
import { InputDateComponent } from './components/formly/input-date/input-date.component';
import { SubmitTaskComponent } from './pages/student/submit-task/submit-task.component';
import { InputFileComponent } from './components/formly/input-file/input-file.component';
import { TaskSubmissionsComponent } from './pages/teacher/task-submissions/task-submissions.component';
import { BypassPipe } from './pipes/bypass.pipe';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { LANG, StoreService } from './services/store.service';
import { SimulatorComponent } from './pages/common/simulator/simulator.component';
import { ErrorInterceptor } from './api/error.interceptor';

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    SettingsComponent,
    ControlPanelComponent,
    StudentHomeComponent,
    TeacherHomeComponent,
    StudentTasksComponent,
    TeacherTasksComponent,
    SubmitTaskComponent,
    TaskSubmissionsComponent,
    BypassPipe,
    SimulatorComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ThemeAppModule.forChild(),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      }
    }),
    BrowserAnimationsModule,
    ChartsModule,
    NgbModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true }
  ],
  bootstrap: [
    AppComponent
  ],
  exports: []
})
export class AppModule {
  constructor(
    _translateService: TranslateService,
    _storeService: StoreService
  ) {
    const lang = _storeService.getItem(LANG);
    if (lang) {
      _translateService.setDefaultLang(lang);
    } else {
      _translateService.setDefaultLang('en');
    }
  }
}
