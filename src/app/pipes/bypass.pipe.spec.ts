import { BypassPipe } from './bypass.pipe';
import { inject, TestBed } from '@angular/core/testing';
import { BrowserModule, DomSanitizer } from '@angular/platform-browser';

describe('BypassPipe', () => {
  beforeEach(() => {
    TestBed
    .configureTestingModule({
      imports: [
        BrowserModule
      ]
    });
  });

  it('create an instance', inject([DomSanitizer], (domSanitizer: DomSanitizer) => {
    const pipe = new BypassPipe(domSanitizer);
    expect(pipe).toBeTruthy();
  }));
});
