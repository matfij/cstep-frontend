import { Component } from '@angular/core';
import { UtilitiesService } from './services/utilities.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  constructor(
    private _utilitiesService: UtilitiesService
  ) {}

  _apiUrl = this._utilitiesService.apiUrl;
}
