import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { UserRole } from '../api/models/enums';
import { RepositoryService } from '../services/repository.service';

@Injectable({
  providedIn: 'root'
})
export class AuthTeacherGuard implements CanActivate {

  constructor(
    private _repositoryService: RepositoryService,
    private _router: Router
  ) {}

  canActivate(): Observable<boolean> | boolean {
    const user = this._repositoryService.getUser();

    if (user !== null && (user.user_role === UserRole.Teacher || user.user_role === UserRole.Admin)) {
      return true;
    } else {
      this._router.navigate(['']);
      return false;
    }
  }

}
