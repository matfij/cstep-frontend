import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AuthTeacherGuard } from './auth-teacher.guard';

describe('AuthReacherGuard', () => {
  let guard: AuthTeacherGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        RouterTestingModule.withRoutes([]),
      ]
    });
    guard = TestBed.inject(AuthTeacherGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
