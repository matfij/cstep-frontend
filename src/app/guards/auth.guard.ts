import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { RepositoryService } from '../services/repository.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(
    private _repositoryService: RepositoryService,
    private _router: Router
  ) {}

  canActivate(): Observable<boolean> | boolean {
    const user = this._repositoryService.getUser();

    if (user !== null) {
      return true;
    } else {
      this._router.navigate(['']);
      return false;
    }
  }

}
