import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { TranslateModule } from '@ngx-translate/core';
import { ThemeAppModule } from '../@theme-app/theme-app.module';
import { AppConstant } from '../api/models/models';
import { UtilitiesService } from './utilities.service';

describe('UtilitiesService', () => {
  let service: UtilitiesService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        TranslateModule.forRoot(),
        ThemeAppModule.forChild()
      ]
    });
    service = TestBed.inject(UtilitiesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should transform enum to const', () => {
    enum Colors {
      Red,
      Green,
      Blue
    }
    const namePrepend = 'resPrefix.';
    const enumConst = service.enumToConst(Colors, { namePrepend: namePrepend });

    const expectedEnumConst: AppConstant[] = [
      { value: 0, name: 'resPrefix.Red' },
      { value: 1, name: 'resPrefix.Green' },
      { value: 2, name: 'resPrefix.Blue' }
    ]
    expect(enumConst).toEqual(expectedEnumConst);
  });
});


