import { Injectable } from '@angular/core';


// user data keys
export const USER_DATA = 'user-data';
export const ACCESS_TOKEN = 'access-token';
export const LANG = 'app-lang';

// tasks data keys
export const CURRENT_TASK = 'current-task';

@Injectable({
  providedIn: 'root'
})
export class StoreService {

  constructor() {}

    public getItem(key: string) {
      return localStorage.getItem(key)
    }

    public setItem(key: string, value: any) {
      return localStorage.setItem(key, value);
    }

    public removeItem(key: string) {
      return localStorage.removeItem(key);
    }

    public clearAll() {
      localStorage.removeItem(USER_DATA);
      localStorage.removeItem(ACCESS_TOKEN);
      localStorage.removeItem(CURRENT_TASK);
    }
}
