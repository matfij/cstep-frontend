import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { AppConstant } from '../api/models/models';

@Injectable({
  providedIn: 'root'
})
export class UtilitiesService {

  constructor(
    private _translateService: TranslateService,
    private _toastrService: ToastrService,
  ) {}

  get apiUrl() {
    return environment.apiUrl;
  }

  enumToConst(enumObj, options?: { namePrepend: string, valueProperty?: string, nameProperty?: string }): AppConstant[] {
    const defaultOptions = { namePrepend: '', valueProperty: 'value', nameProperty: 'name' };
    options = { ...defaultOptions, ...options };

    return Object.keys(enumObj).filter(item => isNaN(Number(item))).map(item => {
      return {
        value: enumObj[item],
        name: this._translateService.instant(options.namePrepend + item)
      };
    });
  }

  toBase64(file: File): Promise<any> {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result.toString());
      reader.onerror = error => reject(error);
    });
  }

  getCurrentDate(): string {
    const currentdate = new Date();

    let day = currentdate.getDate().toString();;
    if (day.length < 2) day = '0' + day;

    let month = (currentdate.getMonth()+1).toString();
    if (month.length < 2) month = '0' + month;

    let year = currentdate.getFullYear().toString();

    return day + "-" + month + "-" + year;
  }

  notifySuccess(title?: string, message?: string) {
    title = title ? this._translateService.instant(title) : null;
    message = message ? this._translateService.instant(message) : null;
    this._toastrService.success(message, title);
  }

  notifyError(title?: string, message?: string) {
    title = title ? this._translateService.instant(title) : null;
    message = message ? this._translateService.instant(message) : null;
    this._toastrService.error(message, title);
  }

  blobToText(blob: any): Observable<string> {
    return new Observable<string>((observer: any) => {
      if (!blob) {
        observer.next('');
        observer.complete();
      } else {
        const reader = new FileReader();
        reader.onload = function() {
          observer.next(this.result);
          observer.complete();
        };
        reader.readAsText(blob, 'deflate');
      }
    });
  }
}
