import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { ApiClient } from '../api/api.client';
import { AccessToken, Task, User } from '../api/models/models';
import { ACCESS_TOKEN, CURRENT_TASK, StoreService, USER_DATA } from './store.service';

@Injectable({
  providedIn: 'root'
})
export class RepositoryService {

  constructor(
    private _apiClient: ApiClient,
    private _storeService: StoreService,
    private _router: Router
  ) {}

  login(user: User) {
    this._storeService.setItem(USER_DATA, JSON.stringify(user));
    this._storeService.setItem(ACCESS_TOKEN, user.token);
  }

  logout() {
    this._storeService.clearAll();
    this._router.navigateByUrl('login');
  }

  getAccessToken() {
    const user = this.getUser();
    return this._apiClient.refreshAccess(user).pipe(
      map((token: AccessToken) => {
        this._storeService.setItem(ACCESS_TOKEN, token.token);
        return token.token;
      })
    );
  }

  getUser(): User {
    const user = this._storeService.getItem(USER_DATA);
    return user ? JSON.parse(user) : null;
  }

  updateUser(user: User) {
    this._storeService.setItem(USER_DATA, JSON.stringify(user));
  }

  getUserFullName(): string {
    const user = this.getUser();
    return user.first_name + ' ' + user.last_name;
  }

  storeCurrentTask(task: Task) {
    this._storeService.setItem(CURRENT_TASK, JSON.stringify(task));
  }

  getCurrentTask(): Task {
    const task = this._storeService.getItem(CURRENT_TASK);
    return task ? JSON.parse(task) : null;
  }

}
