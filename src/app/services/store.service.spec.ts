import { TestBed } from '@angular/core/testing';

import { StoreService } from './store.service';

describe('StoreService', () => {
  let service: StoreService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(StoreService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should store and read value', () => {
    const KEY = 'test-key';
    const VALUE = { 'test value': 100 };
    service.setItem(KEY, JSON.stringify(VALUE));

    const readValue = JSON.parse(service.getItem(KEY));
    expect(readValue).toEqual(VALUE);
  });
});
