import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivePage, UserRole } from 'src/app/api/models/enums';
import { User } from 'src/app/api/models/models';
import { RepositoryService } from 'src/app/services/repository.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  @Input() _activePage: ActivePage = ActivePage.Home;
  _isCollapsed: boolean;
  _user: User;
  _navigationItems: NavigationItem[] = [
    { label: 'navbar.home', href: 'home'},
    { label: 'navbar.tasks', href: 'tasks'},
    { label: 'navbar.controlPanel', href: 'controlPanel'},
    { label: 'navbar.settings', href: 'settings'},
    // { label: 'navbar.simulator', href: 'simulator'}
  ]

  constructor(
    private _repositoryService: RepositoryService,
    private _router: Router
  ) {}

  ngOnInit(): void {
    this._isCollapsed = false;
    this._user = this._repositoryService.getUser();

    if (this._user && this._user.user_role !== UserRole.Student) {
      this._navigationItems.push(
        { label: 'navbar.reports', href: 'taskSubmissions'},
      );
    }
  }

  _getActivePage = ActivePage;

  _navigate(href: string) {
    switch (this._user.user_role) {
      case UserRole.Student: { this._router.navigateByUrl('/student/' + href); break; }
      case UserRole.Teacher: { this._router.navigateByUrl('/teacher/' + href); break; }
      case UserRole.Admin: { this._router.navigateByUrl('/teacher/' + href); break; }
      default: { this._logout(); break; }
    }
  }

  _logout() {
    this._repositoryService.logout();
  }

}

interface NavigationItem {
  label: string;
  href: string;
}
