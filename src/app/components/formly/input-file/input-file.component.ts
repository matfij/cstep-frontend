import { Component } from '@angular/core';
import { FieldType } from '@ngx-formly/core';
import { FileModel } from 'src/app/api/models/models';
import { StoreService } from 'src/app/services/store.service';
import { UtilitiesService } from 'src/app/services/utilities.service';

@Component({
  selector: 'app-input-file',
  templateUrl: './input-file.component.html',
  styleUrls: ['./input-file.component.scss']
})
export class InputFileComponent extends FieldType {

  constructor(
    private _utilitiesService: UtilitiesService,
    private _storeService: StoreService
  ) {
    super();
  }

  async _onFilesChange(files: File[]) {
    const fileBase = await this._utilitiesService.toBase64(files[0]);
    this._storeService.setItem(this.to.store, fileBase);
  }

}
