import { Component, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { FieldWrapper } from '@ngx-formly/core';

@Component({
  selector: 'formly-field-wrapper',
  templateUrl: './field-wrapper.component.html',
  styleUrls: ['./field-wrapper.component.scss'],
})
export class FieldWrapperComponent extends FieldWrapper implements OnInit {

  @ViewChild('fieldComponent', { read: ViewContainerRef, static: false })
  fieldComponent: ViewContainerRef;

  validators;

  ngOnInit() {
    this.validators = this['field'].validators;
  }

  printError(errors) {
    var err = Object.keys(errors)[0];
    if (this.validators && this.validators[err]) {
      return this.validators[err].message;
    } else {
      return err; // todo - complete error string resource path
    }
  }

}
