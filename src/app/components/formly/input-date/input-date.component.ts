import { ViewChild } from '@angular/core';
import { Component } from '@angular/core';
import { FieldType } from '@ngx-formly/core';
import * as moment from 'moment';
import { DatePickerComponent } from 'ng2-date-picker';

@Component({
  selector: 'app-input-date',
  templateUrl: './input-date.component.html',
  styleUrls: ['./input-date.component.scss']
})
export class InputDateComponent extends FieldType {

  @ViewChild('dayPicker') datePicker: DatePickerComponent;
  _config = {
    monthFormat: 'MMM, YYYY',
    disableKeypress: false,
    allowMultiSelect: false,
    closeOnSelect: undefined,
    closeOnSelectDelay: 100,
    onOpenDelay: 0,
    weekDayFormat: 'ddd',
    appendTo: document.body,
    drops: 'top',
    opens: 'right',
    showNearMonthDays: true,
    showWeekNumbers: false,
    enableMonthSelector: true,
    format: "YYYY-MM-DD",
    yearFormat: 'YYYY',
    showGoToCurrent: true,
    dayBtnFormat: 'DD',
    monthBtnFormat: 'MMM',
    hours12Format: 'hh',
    hours24Format: 'HH',
    meridiemFormat: 'A',
    minutesFormat: 'mm',
    minutesInterval: 1,
    secondsFormat: 'ss',
    secondsInterval: 1,
    showSeconds: false,
    showTwentyFourHours: true,
    multipleYearsNavigateBy: 10,
    showMultipleYearsNavigation: false,
    min: moment().format('YYYY-MM-DD'),
  };

  constructor() {
    super();
  }

  _open() {
    this.datePicker.api.open();
  }

  _close() {
    this.datePicker.api.close();
  }

}
