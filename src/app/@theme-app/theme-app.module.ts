import { NgModule, ModuleWithProviders, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormlyModule } from '@ngx-formly/core';
import { RouterModule } from '@angular/router';
import { FieldWrapperComponent } from '../components/formly/field-wrapper/field-wrapper.component';
import { InputTextComponent } from '../components/formly/input-text/input-text.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavbarComponent } from '../components/navbar/navbar.component';
import { SidebarModule } from 'ng-sidebar';
import { DpDatePickerModule } from 'ng2-date-picker';
import { InputTextareaComponent } from '../components/formly/input-textarea/input-textarea.component';
import { InputDateComponent } from '../components/formly/input-date/input-date.component';
import { InputFileComponent } from '../components/formly/input-file/input-file.component';
import { PdfViewerModule } from 'ng2-pdf-viewer';

const CHILD_PROVIDERS = [
  ...TranslateModule.forChild().providers
];

@NgModule({
  declarations: [
    InputTextComponent,
    InputTextareaComponent,
    FieldWrapperComponent,
    InputDateComponent,
    InputFileComponent,
    NavbarComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    CommonModule,
    BrowserAnimationsModule,
    DpDatePickerModule,
    ToastrModule.forRoot(),
    TranslateModule.forChild(),
    FormlyModule.forRoot({
      types: [
        { name: 'input-text', component: InputTextComponent, wrappers: ['form-field'] },
        { name: 'input-textarea', component: InputTextareaComponent, wrappers: ['form-field'] },
        { name: 'input-date', component: InputDateComponent, wrappers: ['form-field'] },
        { name: 'input-file', component: InputFileComponent, wrappers: ['form-field'] }
      ],
      wrappers: [
        { name: 'form-field', component: FieldWrapperComponent }
      ]
    }),
    SidebarModule.forRoot(),
    PdfViewerModule
  ],
  exports: [
    TranslateModule,
    RouterModule,
    ReactiveFormsModule,
    FormlyModule,
    BrowserAnimationsModule,
    ToastrModule,
    NavbarComponent,
    SidebarModule,
    DpDatePickerModule,
    PdfViewerModule
  ],
  providers: [],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ]
})
export class ThemeAppModule {
  public static forChild(): ModuleWithProviders {
    return {
      ngModule: ThemeAppModule,
      providers: [
        ...CHILD_PROVIDERS
      ]
    };
  }
}
