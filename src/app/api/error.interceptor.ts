import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable, EMPTY, throwError } from 'rxjs';
import { catchError, switchMap, take, throttleTime } from 'rxjs/operators';
import { ACCESS_TOKEN, StoreService } from '../services/store.service';
import { RepositoryService } from '../services/repository.service';
import { UtilitiesService } from '../services/utilities.service';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {

  private _LOGIN_REQUEST_URL = '/users/login';

  constructor(
    private _repositoryService: RepositoryService,
    private _utilitiesService: UtilitiesService,
    private _storeService: StoreService
  ) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(catchError(error => {
      if (error.status === 401 && request.url.indexOf(this._LOGIN_REQUEST_URL) !== -1 ) {
        this._repositoryService.logout();
        location.reload();
        return EMPTY;
      } else if (error.status === 401 && request.url.indexOf(this._LOGIN_REQUEST_URL) === -1) {
        const token = this._storeService.getItem(ACCESS_TOKEN);
        if (token) {
          return this._repositoryService.getAccessToken().pipe(
            throttleTime(2000),
            switchMap(freshToken => next.handle(this._applyCredentials(request, freshToken)))
          );
        } else {
          return EMPTY;
          ;
        }
      } else {
        return throwError(error);
      }
    }));
  }

  private _applyCredentials(request: HttpRequest<any>, token: string) {
    return request.clone({
      setHeaders: {
        Authorization: token
      }
    });
  }
}
