import { UserRole } from './enums';

export interface AppConstant {
  value: number;
  name: string;
}

export interface User {
  id?: number;
  login: string;
  password: string;
  email: string;
  first_name: string;
  last_name: string;
  index: string;
  token?: string;
  user_role?: UserRole;
  group_id?: string;
}

export interface SimulationParameters {
  id?: number;
  simulation_parameters_id?: string;
  task_id?: number;
  user_id: number;
  gain_k: number;
  gain_p: number;
  gain_i: number;
}

export interface Task {
  id?: number;
  creator_id: number;
  creator_name: string;
  title?: string;
  description: string;
  group_id: string;
  start_date: string;
  end_date: string;
}

export interface TaskSubmission {
  id?: number;
  task_id: number;
  task_title?: string;
  report_file: string;
  creator_name: string;
  creator_id: number;
  rate: number;
  comment: string;
  submission_date: string;
  rate_date: string;
}

export interface FileModel {
  creator_name: string;
  file: File;
}

export interface LoginParams {
  login: string;
  password: string;
}

export interface ResultMessage {
  message?: string;
  code?: number;
}

export interface StatusMessage {
  status: string;
}

export interface AccessToken {
  token: string;
}

export interface UpdatePasswordParams {
  id: number;
  old_password: string;
  new_password: string;
}
