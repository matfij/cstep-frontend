export enum UserRole {
  Admin,
  Teacher,
  Student
}

export enum ActivePage {
  Home,
  Tasks,
  ControlPanel,
  Settings
}
