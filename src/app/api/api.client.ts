import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { AccessToken, LoginParams, SimulationParameters, StatusMessage, Task, TaskSubmission, UpdatePasswordParams, User } from './models/models';

@Injectable({
  providedIn: 'root',
})
export class ApiClient {

  private _serverUrlPrefix: string;
  private _rbUrlPrefix: string;

  constructor(
    private _httpClient: HttpClient
  ) {
    this._serverUrlPrefix = environment.apiUrl;
    this._rbUrlPrefix = environment.rbUrl;
  }

  //
  // Users controller
  //

  public login(loginParams: LoginParams): Observable<User> {
    const url = this._serverUrlPrefix + '/users/login';
    return this._httpClient.post<User>(url, loginParams);
  }

  public register(user: User): Observable<User> {
    const url = this._serverUrlPrefix + '/users/register';
    return this._httpClient.post<User>(url, user);
  }

  public refreshAccess(user: User): Observable<AccessToken> {
    const url = this._serverUrlPrefix + '/users/refresh';
    return this._httpClient.post<AccessToken>(url, user);
  }

  public createUser(user: User): Observable<User> {
    const url = this._serverUrlPrefix + '/users/add';
    return this._httpClient.post<User>(url, user);
  }

  public updateUserPassword(params: UpdatePasswordParams): Observable<User> {
    const url = this._serverUrlPrefix + '/users/updatePassword';
    return this._httpClient.put<User>(url, params);
  }

  //
  // Tasks controller
  //

  public addTask(task: Task): Observable<Task> {
    const url = this._serverUrlPrefix + '/tasks/add';
    return this._httpClient.post<Task>(url, task);
  }

  public getTasks(): Observable<Task[]> {
    const url = this._serverUrlPrefix + '/tasks/getAll';
    return this._httpClient.get<Task[]>(url);
  }

  public editTask(task: Task): Observable<Task> {
    const url = this._serverUrlPrefix + '/tasks/update';
    return this._httpClient.put<Task>(url, task);
  }

  //
  // Task submissions controller
  //

  public addTaskSubmission(taskSubmission: TaskSubmission): Observable<TaskSubmission> {
    const url = this._serverUrlPrefix + '/taskSubmissions/add';
    return this._httpClient.post<TaskSubmission>(url, taskSubmission);
  }

  public getTaskSubmissions(): Observable<TaskSubmission[]> {
    const url = this._serverUrlPrefix + '/taskSubmissions/getAll';
    return this._httpClient.get<TaskSubmission[]>(url);
  }

  public updateTaskSubmission(taskSubmission: TaskSubmission): Observable<TaskSubmission> {
    const url = this._serverUrlPrefix + '/taskSubmissions/update';
    return this._httpClient.put<TaskSubmission>(url, taskSubmission);
  }

  public getTaskSubmissionDetails(id: number): Observable<TaskSubmission> {
    const url = this._serverUrlPrefix + '/taskSubmissions/get/' + id;
    return this._httpClient.get<TaskSubmission>(url);
  }

  //
  // Raspberry Pi controller
  //

  public getStatus(): Observable<StatusMessage> {
    const url = this._rbUrlPrefix + '/';
    return this._httpClient.get<StatusMessage>(url);
  }

  public setSimulationParameters(simulationParameters: SimulationParameters): Observable<StatusMessage> {
    const url = this._rbUrlPrefix + '/setParameters';
    return this._httpClient.post<StatusMessage>(url, simulationParameters);
  }

}
