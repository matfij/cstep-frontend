import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ACCESS_TOKEN, StoreService } from '../services/store.service';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {

  constructor(
    private _storeService: StoreService
  ) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // demo changes
    const token = this._storeService.getItem(ACCESS_TOKEN);
    if (token) {
      request = this.applyCredentials(request, token);
    }
    return next.handle(request);
  }

  public applyCredentials(request: HttpRequest<any>, token: string) {
    // demo changes
    return request.clone({
      setHeaders: {
        Authorization: token
      }
    });
  }
}
