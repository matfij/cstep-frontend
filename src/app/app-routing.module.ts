import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './pages/common/login/login.component';
import { RegisterComponent } from './pages/common/register/register.component';
import { HomeComponent as TeacherHomeComponent } from './pages/teacher/home/home.component';
import { HomeComponent as StudentHomeComponent } from './pages/student/home/home.component';
import { AuthGuard } from './guards/auth.guard';
import { AuthTeacherGuard } from './guards/auth-teacher.guard';
import { TasksComponent as TeacherTasksComponent } from './pages/teacher/tasks/tasks.component';
import { TasksComponent as StudentTasksComponent } from './pages/student/tasks/tasks.component';
import { ControlPanelComponent } from './pages/common/control-panel/control-panel.component';
import { SettingsComponent } from './pages/common/settings/settings.component';
import { SubmitTaskComponent } from './pages/student/submit-task/submit-task.component';
import { TaskSubmissionsComponent } from './pages/teacher/task-submissions/task-submissions.component';
import { SimulatorComponent } from './pages/common/simulator/simulator.component';


const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'register',
    component: RegisterComponent
  },
  {
    path: 'teacher/home',
    component: TeacherHomeComponent,
    canActivate: [AuthGuard, AuthTeacherGuard]
  },
  {
    path: 'student/home',
    component: StudentHomeComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'teacher/tasks',
    component: TeacherTasksComponent,
    canActivate: [AuthGuard, AuthTeacherGuard]
  },
  {
    path: 'student/tasks',
    component: StudentTasksComponent,
    canActivate: [AuthGuard]
  },
  { path: 'student/controlPanel', redirectTo: 'controlPanel', pathMatch: 'full' },
  { path: 'teacher/controlPanel', redirectTo: 'controlPanel', pathMatch: 'full' },
  {
    path: 'controlPanel',
    component: ControlPanelComponent,
    canActivate: [AuthGuard]
  },
  { path: 'student/settings', redirectTo: 'settings', pathMatch: 'full' },
  { path: 'teacher/settings', redirectTo: 'settings', pathMatch: 'full' },
  {
    path: 'settings',
    component: SettingsComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'student/submitTask',
    component: SubmitTaskComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'teacher/taskSubmissions',
    component: TaskSubmissionsComponent,
    canActivate: [AuthGuard, AuthTeacherGuard]
  },
  { path: 'student/simulator', redirectTo: 'simulator', pathMatch: 'full' },
  { path: 'teacher/simulator', redirectTo: 'simulator', pathMatch: 'full' },
  {
    path: 'simulator',
    component: SimulatorComponent,
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
