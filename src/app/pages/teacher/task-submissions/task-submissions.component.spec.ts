import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaskSubmissionsComponent } from './task-submissions.component';

describe('TaskSubmissionsComponent', () => {
  let component: TaskSubmissionsComponent;
  let fixture: ComponentFixture<TaskSubmissionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaskSubmissionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaskSubmissionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  xit('should create', () => {
    expect(component).toBeTruthy();
  });
});
