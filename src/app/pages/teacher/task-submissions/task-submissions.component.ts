import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { TranslateService } from '@ngx-translate/core';
import { ApiClient } from 'src/app/api/api.client';
import { ResultMessage, Task, TaskSubmission, User } from 'src/app/api/models/models';
import { RepositoryService } from 'src/app/services/repository.service';
import { UtilitiesService } from 'src/app/services/utilities.service';

@Component({
  selector: 'app-task-submissions',
  templateUrl: './task-submissions.component.html',
  styleUrls: ['./task-submissions.component.scss']
})
export class TaskSubmissionsComponent implements OnInit {

  _listLoading: boolean;
  _rateLoading: boolean;
  _previewLoading: boolean;

  _user: User;
  _taskSubissions: TaskSubmission[] = [];
  _previewData: string;
  _rateInd: number;

  _rateModel: TaskSubmission;
  _rateForm = new FormGroup({});
  _rateFields: FormlyFieldConfig[];

  constructor(
    private _apiClient: ApiClient,
    private _utilitiesService: UtilitiesService,
    private _translateService: TranslateService,
    private _repositoryService: RepositoryService
  ) {}

  ngOnInit(): void {
    this._user = this._repositoryService.getUser();
    this._getTaskSubmissions();
  }

  private _getTaskSubmissions() {
    this._listLoading = true;
    this._apiClient.getTasks().subscribe((tasks: Task[]) => {

      const ownedTaskIds = tasks.filter(t => t.creator_id === this._user.id).map(t => { return t.id });

      this._apiClient.getTaskSubmissions().subscribe((taskSubmissions: TaskSubmission[]) => {
        this._listLoading = false;
        taskSubmissions = taskSubmissions.filter(t => ownedTaskIds.indexOf(t.task_id) > -1);
        this._taskSubissions = taskSubmissions.reverse();
      }, (error: HttpErrorResponse) => {
        this._listLoading = false;
        this._utilitiesService.notifyError('tasks.taskSubmissionFail', error.error.message);
      });

    }, (error: HttpErrorResponse) => {
      this._listLoading = false;
      this._utilitiesService.notifyError('tasks.taskSubmissionFail', error.error.message);
    });
  }

  _displayRateForm(index: number) {
    this._rateInd = index;
    this._rateFields = this._createRateFields();
    this._rateModel = this._taskSubissions[index];
  }

  private _createRateFields(): FormlyFieldConfig[] {
    return [
      {
        fieldGroupClassName: 'row',
        fieldGroup: [
          {
            className: 'col-12',
            key: 'rate',
            type: 'input-text',
            templateOptions: {
              label: this._translateService.instant('tasks.rate'),
              required: true,
              type: 'number',
              max: 5,
              min: 0
            },
          },
          {
            className: 'col-12',
            key: 'comment',
            type: 'input-text',
            templateOptions: {
              label: this._translateService.instant('tasks.comment')
            },
          }
        ]
      }
    ]
  }

  _displayDetails(id: number) {
    this._previewLoading = true;
    this._apiClient.getTaskSubmissionDetails(id).subscribe((taskSubmission: TaskSubmission) => {
      this._previewLoading = false;
      this._previewData = taskSubmission.report_file.replace('data:application/pdf;base64,', 'data:image/png;base64,');
    }, (error: HttpErrorResponse) => {
      this._previewLoading = false;
      this._utilitiesService.notifyError('tasks.taskSubmissionFail', error.error.message);
    });
  }

  _rateSubmission() {
    if (this._rateForm.valid) {
      this._rateLoading = true;
      this._apiClient.updateTaskSubmission(this._rateModel).subscribe(_ => {
        this._rateLoading = false;
        this._taskSubissions.splice(this._rateInd, 1);
        this._rateInd = null;
        this._previewData = null;
      }, (error: HttpErrorResponse) => {
        this._rateLoading = false;
        this._utilitiesService.notifyError('tasks.rateError');
      });
    } else {
      this._utilitiesService.notifyError('tasks.rateMissing');
    }
  }

  _cancelRating() {
    this._rateInd = null;
  }
}
