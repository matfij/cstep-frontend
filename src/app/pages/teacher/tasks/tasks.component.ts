import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { TranslateService } from '@ngx-translate/core';
import { ApiClient } from 'src/app/api/api.client';
import { Task, User } from 'src/app/api/models/models';
import { RepositoryService } from 'src/app/services/repository.service';
import { UtilitiesService } from 'src/app/services/utilities.service';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.scss']
})
export class TasksComponent implements OnInit {

  _taskModel = {} as Task;
  _taskForm = new FormGroup({});
  _taskFields: FormlyFieldConfig[];

  _editMode: boolean;
  _editedInd: number;
  _formLoading: boolean;
  _listLoading: boolean;

  _user: User;
  _tasks: Task[] = [];

  constructor(
    private _apiClient: ApiClient,
    private _utilitiesService: UtilitiesService,
    private _translateService: TranslateService,
    private _repositoryService: RepositoryService
  ) {}

  ngOnInit(): void {
    this._editMode = false;
    this._user = this._repositoryService.getUser();
    this._taskFields = this._createTaskFields();
    this._getTasks();
  }

  private _createTaskFields(): FormlyFieldConfig[] {
    return [
      {
        fieldGroupClassName: 'row',
        fieldGroup: [
          {
            className: 'col-12',
            key: 'title',
            type: 'input-text',
            templateOptions: {
              label: this._translateService.instant('tasks.title'),
              required: true
            },
          },
          {
            className: 'col-12',
            key: 'group_id',
            type: 'input-text',
            templateOptions: {
              label: this._translateService.instant('tasks.groupId'),
            },
          },
          {
            className: 'col-12',
            key: 'description',
            type: 'input-textarea',
            templateOptions: {
              required: true,
              label: this._translateService.instant('tasks.description'),
            },
          },
          {
            className: 'col-12 mt-0 mb-0 pb-0',
            key: 'start_date',
            type: 'input-date',
            templateOptions: {
              required: true,
              label: this._translateService.instant('tasks.startDate'),
            },
          },
          {
            className: 'col-12 mt-0',
            key: 'end_date',
            type: 'input-date',
            templateOptions: {
              required: true,
              label: this._translateService.instant('tasks.endDate'),
            }
          }
        ]
      }
    ]
  }

  private _getTasks() {
    this._listLoading = true;
    this._apiClient.getTasks().subscribe((tasks: Task[]) => {
      this._listLoading = false;
      this._tasks = tasks.filter(t => t.creator_id === this._user.id).reverse();
    }, (error) => {
      this._listLoading = false;
      this._utilitiesService.notifyError('tasks.getTasksError', error.message);
    });
  }

  _submit() {
    if (this._taskForm.valid) {
      this._formLoading = true;

      if (this._editMode) {
        this._apiClient.editTask(this._taskModel).subscribe((task: Task) => {
          this._formLoading = false;
          this._taskForm.reset();
          this._tasks[this._editedInd] = task;
          this._editMode = false;
        }, (error) => {
          this._formLoading = false;
          this._utilitiesService.notifyError('tasks.uploadingTaskError', error.error.message);
        });
      } else {
        this._taskModel.creator_name = this._repositoryService.getUserFullName();
        this._taskModel.creator_id = this._user.id;

        this._apiClient.addTask(this._taskModel).subscribe((task: Task) => {
          this._formLoading = false;
          this._taskForm.reset();
          this._tasks.unshift(task);
        }, (error) => {
          this._formLoading = false;
          this._utilitiesService.notifyError('tasks.uploadingTaskError', error.error.message);
        });
      }
    } else {
      this._utilitiesService.notifyError('tasks.addTaskError', 'toasts.fillRequiredFields');
    }
  }

  _edit(taskInd: number) {
    this._editMode = true;
    this._editedInd = taskInd;
    this._taskModel = {
      id: this._tasks[taskInd].id,
      creator_id: this._tasks[taskInd].creator_id,
      creator_name: this._tasks[taskInd].creator_name,
      description: this._tasks[taskInd].description,
      start_date: this._tasks[taskInd].start_date,
      end_date: this._tasks[taskInd].end_date,
      group_id: this._tasks[taskInd].group_id,
      title: this._tasks[taskInd].title
    };
  }

  _cancelEdit() {
    this._editMode = false;
    this._taskForm.reset();
  }

  _delete(taskInd: number) {

  }

}
