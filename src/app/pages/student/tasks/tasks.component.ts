import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { ApiClient } from 'src/app/api/api.client';
import { Task, TaskSubmission, User } from 'src/app/api/models/models';
import { RepositoryService } from 'src/app/services/repository.service';
import { UtilitiesService } from 'src/app/services/utilities.service';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.scss']
})
export class TasksComponent implements OnInit {

  _listLoading: boolean;

  _user: User;
  _tasks: Task[] = [];
  _taskSubmissions: TaskSubmission[] = [];
  _taskSubmissionIds: number[] = [];

  constructor(
    private _apiClient: ApiClient,
    private _utilitiesService: UtilitiesService,
    private _repositoryService: RepositoryService,
    private _router: Router
  ) {}

  ngOnInit(): void {
    this._user = this._repositoryService.getUser();
    this._getTasks();
  }

  private _getTasks() {
    this._listLoading = true;
    this._apiClient.getTasks().subscribe((tasks: Task[]) => {
      this._getTaskSubmissions();
      this._tasks = tasks.reverse();
    }, (error: HttpErrorResponse) => {
      this._listLoading = false;
      this._utilitiesService.notifyError('tasks.getTasksError', error.error.message);
    });
  }

  private _getTaskSubmissions() {
    this._apiClient.getTaskSubmissions().subscribe((taskSubumissions: TaskSubmission[]) => {
      taskSubumissions = taskSubumissions.filter(t => t.creator_id === this._user.id);
      this._taskSubmissions = taskSubumissions.filter(t => t.creator_id === this._user.id);
      this._taskSubmissionIds = taskSubumissions.map(t => { return t.task_id });
      this._listLoading = false;
    }, (_) => {});
  }

  _submit(taskInd: number) {
    const currentTask = this._tasks[taskInd];
    this._repositoryService.storeCurrentTask(currentTask);
    this._router.navigateByUrl('/student/submitTask');
  }

}
