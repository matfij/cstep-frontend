import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { TranslateService } from '@ngx-translate/core';
import { ApiClient } from 'src/app/api/api.client';
import { FileModel, Task, TaskSubmission, User } from 'src/app/api/models/models';
import { RepositoryService } from 'src/app/services/repository.service';
import { StoreService } from 'src/app/services/store.service';
import { UtilitiesService } from 'src/app/services/utilities.service';

@Component({
  selector: 'app-submit-task',
  templateUrl: './submit-task.component.html',
  styleUrls: ['./submit-task.component.scss']
})
export class SubmitTaskComponent implements OnInit {

  _user: User;
  _currentTask: Task;

  _loading: boolean;

  _fileModel = {} as FileModel;
  _taskSubmissionModel = {} as TaskSubmission;
  _fileForm = new FormGroup({});
  _fileFields: FormlyFieldConfig[];

  readonly SUBMISSION_FILE = 'submission-file';

  constructor(
    private _apiClient: ApiClient,
    private _repositoryService: RepositoryService,
    private _translateService: TranslateService,
    private _utilitiesService: UtilitiesService,
    private _storeService: StoreService,
    private _router: Router
  ) {}

  ngOnInit(): void {
    this._user = this._repositoryService.getUser();
    this._currentTask = this._repositoryService.getCurrentTask();
    this._fileFields = this._createfileFields();
    this._fileModel.creator_name = this._repositoryService.getUserFullName();
  }

  _createfileFields(): FormlyFieldConfig[] {
    return [
      {
        fieldGroupClassName: 'row',
        fieldGroup: [
          {
            className: 'col-12',
            key: 'creator_name',
            type: 'input-text',
            templateOptions: {
              label: this._translateService.instant('tasks.author'),
              disabled: true
            },
          },
          {
            className: 'col-12',
            key: 'file',
            type: 'input-file',
            templateOptions: {
              label: this._translateService.instant('tasks.uploadReportFile'),
              required: true,
              accept: '.pdf',
              store: this.SUBMISSION_FILE
            },
          }
        ]
      }
    ];
  }

  _submit() {
    if (this._fileForm.valid) {
      this._taskSubmissionModel.task_id = this._currentTask.id;
      this._taskSubmissionModel.task_title = this._currentTask.title;
      this._taskSubmissionModel.creator_name = this._repositoryService.getUserFullName();
      this._taskSubmissionModel.creator_id = this._user.id;
      this._taskSubmissionModel.submission_date = this._utilitiesService.getCurrentDate();
      this._taskSubmissionModel.report_file = this._storeService.getItem(this.SUBMISSION_FILE);

      this._loading = true;
      this._apiClient.addTaskSubmission(this._taskSubmissionModel).subscribe(_ => {
        this._loading = false;
        this._utilitiesService.notifySuccess('tasks.taskSubmissionSuccess');
        this._router.navigateByUrl('/student/tasks');
      }, (error: HttpErrorResponse) => {
        this._loading = false;
        this._utilitiesService.notifyError('tasks.taskSubmissionFail', error.error.message);
      })
    } else {
      this._utilitiesService.notifyError('tasks.taskSubmissionFail', 'toasts.fillRequiredFields');
    }
  }

  _cancel() {
    this._router.navigateByUrl('/student/tasks');
  }

}
