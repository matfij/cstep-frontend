import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { ChartDataSets } from 'chart.js'
import { Label } from 'ng2-charts';
import { User } from 'src/app/api/models/models';
import { RepositoryService } from 'src/app/services/repository.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  _user: User;
  _studentGradeData: ChartDataSets[];
  _studentGradeLabels: Label[];

  constructor(
    private _translateService: TranslateService,
    private _repositoryService: RepositoryService,
    private _router: Router
  ) {}

  ngOnInit(): void {
    this._user = this._repositoryService.getUser();
    this._getMarksData();
  }

  _navigateTasks() {
    this._router.navigateByUrl('student/tasks');
  }

  _navigateControlpanel() {
    this._router.navigateByUrl('student/controlPanel');
  }

  private _getMarksData() {
    const studentGradesLabel = this._translateService.instant('home.yourGrades');
    const groupGradesLabel = this._translateService.instant('home.yourGroupGrades');

    this._studentGradeData = [
      { label: studentGradesLabel, data: [3.5, 3.0, 4.0, 5.0, 4.5, 4.0, 0] },
      { label: groupGradesLabel, data: [3.1, 3.3, 3.9, 4.7, 4.6, 3.8, 3.0] }
    ];
    this._studentGradeLabels = ['No. 1', 'No. 2', 'No. 3', 'No. 4', 'No. 5', 'No. 6', 'No. 7'];
  }

}
