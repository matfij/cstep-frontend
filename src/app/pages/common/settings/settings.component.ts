import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ApiClient } from 'src/app/api/api.client';
import { UpdatePasswordParams, User } from 'src/app/api/models/models';
import { AVAILABLE_LANGUAGES, AVAILABLE_LANGUAGES_NAMES, MIN_PASSWORD_LENGTH, MIN_STRING_LENGTH } from 'src/app/services/constants.service';
import { RepositoryService } from 'src/app/services/repository.service';
import { LANG, StoreService } from 'src/app/services/store.service';
import { UtilitiesService } from 'src/app/services/utilities.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {

  private _user: User;

  _availableLangNames = AVAILABLE_LANGUAGES_NAMES;

  _languageCollapsed: boolean;
  _passwordLoading: boolean;

  constructor(
    private _apiClient: ApiClient,
    private _repositoryService: RepositoryService,
    private _utilitiesService: UtilitiesService,
    private _translateService: TranslateService,
    private _storeService: StoreService
  ) {}

  ngOnInit(): void {
    this._languageCollapsed = true;
    this._user = this._repositoryService.getUser();
  }

  _updateLang(index: number) {
    this._languageCollapsed = !this._languageCollapsed

    const newLang = AVAILABLE_LANGUAGES[index];
    this._translateService.setDefaultLang(newLang);

    this._storeService.setItem(LANG, newLang);
  }

  _checkPassword(password: string) {
    return password ? password : '';
  }

  _updatePassword(oldPassword: string, newPassword: string, checkPassword: string) {
    if (oldPassword.length < MIN_STRING_LENGTH || newPassword.length < MIN_STRING_LENGTH || checkPassword.length < MIN_STRING_LENGTH) {
      this._utilitiesService.notifyError('toasts.fillRequiredFields');
      return;
    }
    if (newPassword !== checkPassword) {
      this._utilitiesService.notifyError('settings.passwordsNotMatch');
      return;
    }
    if (newPassword.length < MIN_PASSWORD_LENGTH) {
      this._utilitiesService.notifyError('settings.passwordTooShort');
      return;
    }

    const params: UpdatePasswordParams = {
      id: this._user.id,
      old_password: oldPassword,
      new_password: newPassword
    };

    this._passwordLoading = true;
    this._apiClient.updateUserPassword(params).subscribe((user: User) => {
      this._passwordLoading = false;
      this._user = user;
      this._repositoryService.updateUser(user);
      this._utilitiesService.notifySuccess('settings.updatedPasswordSuccessfully');
    }, (error: HttpErrorResponse) => {
      this._passwordLoading = false;
      this._user.password = oldPassword;
      this._utilitiesService.notifyError('toasts.connectionError', error.error.message);
    });
  }

}
