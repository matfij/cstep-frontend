import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { TranslateService } from '@ngx-translate/core';
import { ApiClient } from 'src/app/api/api.client';
import { UserRole } from 'src/app/api/models/enums';
import { LoginParams, User } from 'src/app/api/models/models';
import { RepositoryService } from 'src/app/services/repository.service';
import { UtilitiesService } from 'src/app/services/utilities.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  _loginModel = {} as LoginParams;
  _loginForm = new FormGroup({});
  _loginFields: FormlyFieldConfig[];

  _loading: boolean;

  constructor(
    private _apiClient: ApiClient,
    private _utilitiesService: UtilitiesService,
    private _translateService: TranslateService,
    private _repositoryService: RepositoryService,
    private _router: Router
  ) {}

  ngOnInit(): void {
    this._loginFields = this._createLoginFields();
  }

  private _createLoginFields(): FormlyFieldConfig[] {
    return [
      {
        fieldGroupClassName: 'row',
        fieldGroup: [
          {
            className: 'col-12',
            key: 'login',
            type: 'input-text',
            templateOptions: {
              required: true,
              label: this._translateService.instant('login.login')
            },
          },
          {
            className: 'col-12',
            key: 'password',
            type: 'input-text',
            templateOptions: {
              required: true,
              label: this._translateService.instant('login.password'),
              type: 'password'
            },
          }
        ]
      }
    ];
  }

  _submit() {
    if (this._loginForm.valid) {
      this._loading = true;
      this._apiClient.login(this._loginModel).subscribe((user: User) => {
        this._loading = false;
        this._utilitiesService.notifySuccess('login.loginSuccess');
        this._repositoryService.login(user);
        switch (user.user_role) {
          case UserRole.Student: { this._router.navigate(['/student/home']); break; }
          case UserRole.Teacher: { this._router.navigate(['/teacher/home']); break; }
          case UserRole.Admin: { this._router.navigate(['/teacher/home']); break; }
        }
      }, (error: HttpErrorResponse) => {
        this._loading = false;
        this._utilitiesService.notifyError('login.loginError', error.error.message);
      });
    } else {
      this._utilitiesService.notifyError('login.loginError', 'toasts.fillRequiredFields');
    }
  }

}
