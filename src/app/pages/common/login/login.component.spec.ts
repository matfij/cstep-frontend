import { HttpClientTestingModule } from '@angular/common/http/testing';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { FormlyModule } from '@ngx-formly/core';
import { TranslateModule } from '@ngx-translate/core';
import { ToastrModule } from 'ngx-toastr';
import { ThemeAppModule } from 'src/app/@theme-app/theme-app.module';
import { FieldWrapperComponent } from 'src/app/components/formly/field-wrapper/field-wrapper.component';
import { InputDateComponent } from 'src/app/components/formly/input-date/input-date.component';
import { InputFileComponent } from 'src/app/components/formly/input-file/input-file.component';
import { InputTextComponent } from 'src/app/components/formly/input-text/input-text.component';
import { InputTextareaComponent } from 'src/app/components/formly/input-textarea/input-textarea.component';

import { LoginComponent } from './login.component';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        LoginComponent
      ],
      imports: [
        HttpClientTestingModule,
        RouterTestingModule.withRoutes([]),
        TranslateModule.forRoot(),
        ToastrModule.forRoot(),
        FormsModule,
        ReactiveFormsModule,
        ThemeAppModule.forChild(),
        FormlyModule.forRoot({
          types: [
            { name: 'input-text', component: InputTextComponent, wrappers: ['form-field'] },
            { name: 'input-textarea', component: InputTextareaComponent, wrappers: ['form-field'] },
            { name: 'input-date', component: InputDateComponent, wrappers: ['form-field'] },
            { name: 'input-file', component: InputFileComponent, wrappers: ['form-field'] }
          ],
          wrappers: [
            { name: 'form-field', component: FieldWrapperComponent }
          ]
        }),
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
