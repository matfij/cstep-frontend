import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { TranslateService } from '@ngx-translate/core';
import { ApiClient } from 'src/app/api/api.client';
import { SimulationParameters, StatusMessage } from 'src/app/api/models/models';
import { RepositoryService } from 'src/app/services/repository.service';
import { UtilitiesService } from 'src/app/services/utilities.service';

@Component({
  selector: 'app-control-panel',
  templateUrl: './control-panel.component.html',
  styleUrls: ['./control-panel.component.scss']
})
export class ControlPanelComponent implements OnInit {

  _cameraUrl = 'https://smarthomeej.ddns.net/?fbclid=IwAR2D15id_4sZ6sMoV3VbmGA_gkzYyn3pSWUrBE5n3hqk29kk8AsrZ76GUAU';

  _formLoading: boolean;

  _simulationParametersModel = {} as SimulationParameters;
  _simulationParametersForm = new FormGroup({});
  _simulationParametersFields: FormlyFieldConfig[];

  constructor(
    private _apiClient: ApiClient,
    private _repositoryService: RepositoryService,
    private _translateService: TranslateService,
    private _utilitiesService: UtilitiesService
  ) {}

  ngOnInit(): void {
    this._getRaspberryPiStatus();
    this._simulationParametersFields = this._createSimulationParametersFields();
  }

  private _getRaspberryPiStatus() {
    this._apiClient.getStatus().subscribe((statusMessage: StatusMessage) => {
      this._formLoading = false;
      this._utilitiesService.notifySuccess('toasts.updateError', statusMessage.status);
    }, (error: HttpErrorResponse) => {
      this._formLoading = false;
      this._utilitiesService.notifyError('toasts.connectionError', 'controlPanel.connectionError');
    });
  }

  private _createSimulationParametersFields(): FormlyFieldConfig[] {
    return [
      {
        fieldGroupClassName: 'row',
        fieldGroup: [
          {
            className: 'col-12',
            key: 'gain_k',
            type: 'input-text',
            templateOptions: {
              label: this._translateService.instant('controlPanel.gainProportional'),
              type: 'number',
              required: true
            },
          },
          {
            className: 'col-12',
            key: 'gain_p',
            type: 'input-text',
            templateOptions: {
              label: this._translateService.instant('controlPanel.gainIntegral'),
              type: 'number',
              required: true
            },
          },
          {
            className: 'col-12',
            key: 'gain_i',
            type: 'input-text',
            templateOptions: {
              label: this._translateService.instant('controlPanel.gainDerivative'),
              type: 'number',
              required: true
            },
          }
        ]
      }
    ]
  }

  _submit() {
    if (this._simulationParametersForm.valid) {
      this._simulationParametersModel.user_id = this._repositoryService.getUser().id;

      this._formLoading = true;
      this._apiClient.setSimulationParameters(this._simulationParametersModel).subscribe((statusMessage: StatusMessage) => {
        this._formLoading = false;
        this._utilitiesService.notifySuccess('controlPanel.connectionSuccess', statusMessage.status);
      }, (error: HttpErrorResponse) => {
        this._formLoading = false;
        this._utilitiesService.notifyError('toasts.updateError', 'controlPanel.connectionError');
      });
    } else {
      this._utilitiesService.notifyError('controlPanel.formError', 'toasts.fillRequiredFields');
    }
  }

}
