import { Component, OnInit } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { TranslateService } from '@ngx-translate/core';
import { ApiClient } from 'src/app/api/api.client';
import { User } from 'src/app/api/models/models';
import { UtilitiesService } from 'src/app/services/utilities.service';
import { EMAIL_PATTERN, INDEX_LENGTH } from 'src/app/services/constants.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  _registerModel = {} as User;
  _registerForm = new FormGroup({});
  _registerFields: FormlyFieldConfig[];

  _loading: boolean;

  constructor(
    private _apiClient: ApiClient,
    private _utilitiesService: UtilitiesService,
    private _translateService: TranslateService,
    private _router: Router,
    private _httpClient: HttpClient
  ) {}

  ngOnInit(): void {
    this._registerFields = this._createRegisterFields();
  }

  private _createRegisterFields(): FormlyFieldConfig[] {
    return [
      {
        fieldGroupClassName: 'row',
        fieldGroup: [
          {
            className: 'col-12',
            key: 'login',
            type: 'input-text',
            templateOptions: {
              required: true,
              label: this._translateService.instant('login.login')
            },
          },
          {
            className: 'col-12',
            key: 'password',
            type: 'input-text',
            templateOptions: {
              required: true,
              label: this._translateService.instant('login.password'),
              type: 'password'
            },
          },
          {
            className: 'col-12',
            key: 'email',
            type: 'input-text',
            templateOptions: {
              required: true,
              label: this._translateService.instant('register.email'),
              pattern: EMAIL_PATTERN
            },
          },
          {
            className: 'col-12',
            key: 'first_name',
            type: 'input-text',
            templateOptions: {
              required: true,
              label: this._translateService.instant('register.firstName')
            },
          },
          {
            className: 'col-12',
            key: 'last_name',
            type: 'input-text',
            templateOptions: {
              required: true,
              label: this._translateService.instant('register.lastName')
            },
          },
          {
            className: 'col-12',
            key: 'index',
            type: 'input-text',
            templateOptions: {
              required: true,
              label: this._translateService.instant('register.index'),
              type: 'number',
              minLength: INDEX_LENGTH,
              maxLength: INDEX_LENGTH
            },
          }
        ]
      }
    ];
  }

  submit() {
    if (this._registerForm.valid) {
      this._loading = true;
      this._apiClient.register(this._registerModel).subscribe((user: User) => {
        this._loading = false;
        this._utilitiesService.notifySuccess('register.registerSuccess');
        this._router.navigate(['/login']);
      }, (error: HttpErrorResponse) => {
        this._loading = false;
        this._utilitiesService.notifyError('register.registerError', error.error.message);
      });
    } else {
      this._utilitiesService.notifyError('register.registerError', 'toasts.fillRequiredFields');
    }
  }

  testHttp() {
    const url = 'http://51.140.11.172:443/api/core/abstract/';
    const params = {
      "phrase": "transformer language model",
      "page_number": 10,
      "answer_model": 1,
      "summary_model": 1
    };
    this._httpClient.post(url, params, { headers: {'key': 'super'} });
  }

}
