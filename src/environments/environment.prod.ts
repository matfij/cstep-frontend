export const environment = {
  production: true,
  apiUrl: 'https://cstep-api.herokuapp.com/api/v1',
  rbUrl: 'http://smarthomeej.ddns.net:8000'
};
