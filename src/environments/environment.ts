// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.

export const environment = {
  production: false,
  apiUrl: 'https://cstep-api.herokuapp.com/api/v1', //'http://127.0.0.1:5000/api/v1',
  rbUrl: 'http://smarthomeej.ddns.net:4445'
};
